const mongoose = require('mongoose');

const { Schema } = mongoose;

const SearchSchema = new Schema(
  {
    results: Array,
    searchQuery: String,
    searchTitle: String,
    downloadFilter: Object,
    videoOptionDefaults: Object,
    defaultConditionType: String,
    defaultConditionParam: Schema.Types.Mixed,
    certifiedAllResults: Boolean,
    categories: String,
  },
  {
    timestamps: true,
  },
);

const search = mongoose.model('search', SearchSchema);

module.exports = search;
